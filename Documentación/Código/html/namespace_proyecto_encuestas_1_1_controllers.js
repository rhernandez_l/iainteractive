var namespace_proyecto_encuestas_1_1_controllers =
[
    [ "Api", "namespace_proyecto_encuestas_1_1_controllers_1_1_api.html", "namespace_proyecto_encuestas_1_1_controllers_1_1_api" ],
    [ "AccountController", "class_proyecto_encuestas_1_1_controllers_1_1_account_controller.html", "class_proyecto_encuestas_1_1_controllers_1_1_account_controller" ],
    [ "CategoriaController", "class_proyecto_encuestas_1_1_controllers_1_1_categoria_controller.html", "class_proyecto_encuestas_1_1_controllers_1_1_categoria_controller" ],
    [ "EncuestaController", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller" ],
    [ "HomeController", "class_proyecto_encuestas_1_1_controllers_1_1_home_controller.html", "class_proyecto_encuestas_1_1_controllers_1_1_home_controller" ],
    [ "ManageController", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller" ],
    [ "PreguntasController", "class_proyecto_encuestas_1_1_controllers_1_1_preguntas_controller.html", "class_proyecto_encuestas_1_1_controllers_1_1_preguntas_controller" ],
    [ "StatusController", "class_proyecto_encuestas_1_1_controllers_1_1_status_controller.html", "class_proyecto_encuestas_1_1_controllers_1_1_status_controller" ]
];