var hierarchy =
[
    [ "ApiController", null, [
      [ "ProyectoEncuestas.Controllers.Api.EncuestasController", "class_proyecto_encuestas_1_1_controllers_1_1_api_1_1_encuestas_controller.html", null ]
    ] ],
    [ "Controller", null, [
      [ "ProyectoEncuestas.Controllers.AccountController", "class_proyecto_encuestas_1_1_controllers_1_1_account_controller.html", null ],
      [ "ProyectoEncuestas.Controllers.CategoriaController", "class_proyecto_encuestas_1_1_controllers_1_1_categoria_controller.html", null ],
      [ "ProyectoEncuestas.Controllers.EncuestaController", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html", null ],
      [ "ProyectoEncuestas.Controllers.HomeController", "class_proyecto_encuestas_1_1_controllers_1_1_home_controller.html", null ],
      [ "ProyectoEncuestas.Controllers.ManageController", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html", null ],
      [ "ProyectoEncuestas.Controllers.PreguntasController", "class_proyecto_encuestas_1_1_controllers_1_1_preguntas_controller.html", null ],
      [ "ProyectoEncuestas.Controllers.StatusController", "class_proyecto_encuestas_1_1_controllers_1_1_status_controller.html", null ]
    ] ]
];