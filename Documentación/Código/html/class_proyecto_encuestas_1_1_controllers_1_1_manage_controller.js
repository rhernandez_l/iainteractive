var class_proyecto_encuestas_1_1_controllers_1_1_manage_controller =
[
    [ "ManageMessageId", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084", [
      [ "AddPhoneSuccess", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084af08d36b2147213fadb3b223d149ac802", null ],
      [ "ChangePasswordSuccess", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084a0053618c57496fec45c67daf91919394", null ],
      [ "SetTwoFactorSuccess", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084af17987d9118fd576fed3cbec8d11bd8b", null ],
      [ "SetPasswordSuccess", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084af17ca72fcf8adaa46d5e87b12ec3a27d", null ],
      [ "RemoveLoginSuccess", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084a46e53931d0e837638a46368b2b04ba6a", null ],
      [ "RemovePhoneSuccess", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084a70460d8db9c29752c1f591eb67a60fdc", null ],
      [ "Error", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#acfa8d4b0a6fdd66fe79a9dc116449084a902b0d55fddef6f8d651fe1035b7d4bd", null ]
    ] ],
    [ "ManageController", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#ab0f93558088f49ca5395e549f1927453", null ],
    [ "ManageController", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#af8ab0aa258396b018ae794a512fd9e85", null ],
    [ "AddPhoneNumber", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a3e36a82c59ac3d46379321007f50f541", null ],
    [ "AddPhoneNumber", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#aaa72fbca57b08c679446fd8b32dfcbf7", null ],
    [ "ChangePassword", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#afb28c3b08f7c83723d177bacfbc14b8e", null ],
    [ "ChangePassword", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#af21ccb73b0e3248dfe7aa33807b62ee9", null ],
    [ "DisableTwoFactorAuthentication", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#aeac00810f4caa13144e8f8728a6cb454", null ],
    [ "Dispose", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a6f33e4729f96ff7ec6ea851359374dbe", null ],
    [ "EnableTwoFactorAuthentication", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#adf2e778844347129ce7451376d16ba8e", null ],
    [ "Index", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a814c9050a5427dd2dd07464559aad6c0", null ],
    [ "LinkLogin", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#adb5e83f2eb403a158ef07be383d6cbda", null ],
    [ "LinkLoginCallback", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a8c597c467e86ae9db06a2edf32eb0f7a", null ],
    [ "ManageLogins", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a5e6ae8eeb3d1be1ac2d76e026877e3eb", null ],
    [ "RemoveLogin", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a45b0896ea58b5ad5e445c894f00c2303", null ],
    [ "RemovePhoneNumber", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a4798d0ff5a6d1f19dab9d95c1070f22d", null ],
    [ "SetPassword", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a50e5be9724bd430bde77734fd54d2100", null ],
    [ "SetPassword", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a42cad000ce40f6f36dd6f930c9bb8c69", null ],
    [ "VerifyPhoneNumber", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#af4134d92b8e11a836f04f4782254855b", null ],
    [ "VerifyPhoneNumber", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a5ed3a814bab7a0f022552db76d8f3745", null ],
    [ "AuthenticationManager", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#af89d53f9ffa88a52259e510dbe112329", null ],
    [ "SignInManager", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#a23e89b60b942e3600bf7d7a15e35b78f", null ],
    [ "UserManager", "class_proyecto_encuestas_1_1_controllers_1_1_manage_controller.html#ab293af3de0e8e9adb025afb77e2b47e8", null ]
];