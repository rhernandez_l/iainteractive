var class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller =
[
    [ "Activas", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#aa9e41795dc2ad6e30561964824536694", null ],
    [ "Admin", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#ad0288e4db393857f0c3fcdf9e4e89885", null ],
    [ "Create", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#abf4b23e4ccfd2d7ed29ece73237b43d9", null ],
    [ "Create", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#aa2edefa6e50ac1e71256116a5441f349", null ],
    [ "Delete", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#a906594486502a325df3032066c30902e", null ],
    [ "Details", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#adfd3db0018e7358f77722cdea6d3c237", null ],
    [ "Edit", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#af8c0d8e1cd7d8f8f7cc5193e820eb6c1", null ],
    [ "Edit", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#a4aa9ae8ca9e094ddd74c6e9d38ca8644", null ],
    [ "FormAnswer", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#a912bac223d16aa253db2e8f30e4bd7fd", null ],
    [ "FormAnswer", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#abc7792ee548f5485ef679a0a73048ed5", null ],
    [ "MyAnswers", "class_proyecto_encuestas_1_1_controllers_1_1_encuesta_controller.html#a3e5ebe9243eaa6151c69c38ae2142373", null ]
];