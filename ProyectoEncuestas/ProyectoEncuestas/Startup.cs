﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProyectoEncuestas.Startup))]
namespace ProyectoEncuestas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
