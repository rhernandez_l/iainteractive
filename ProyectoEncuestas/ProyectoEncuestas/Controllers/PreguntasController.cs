﻿using ProyectoEncuestas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoEncuestas.Controllers
{
    public class PreguntasController : Controller
    {
        // GET: /Preguntas  
        // Muestra la lista de preguntas
        // Solo los Administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Index(int id = 0)
        {
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin", "Encuesta");
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * Buscamos la encuesta con el id recibido por GET y almacenamos el nombre de la encuesta en ViewBag junto con su id para despues mostrarlos en la vista
                 * Obtenemos todas las preguntas que pertenecen a una encuesta y enviamos el resultado a la vista
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var encuesta = db.encuestas.Find(id);

                    ViewBag.encuestaId = id;
                    ViewBag.encuestaName = encuesta.nombre;

                    var pregunta = db.preguntas.Where(a => a.encuesta_id.Equals(id)).ToList();
                    return View(pregunta);
                }
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al capturar las preguntas " + ex.Message);
                return RedirectToAction("Admin","Encuesta");
            }
        }

        // IndexPartial, Pregutnas, {ID encuesta}
        // Es una vista parcial que muestra las preguntas sin la información de la encuesta (para poder reutilizarla en otra vista)
        // Solo los Administradores tienen acceso
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult IndexPartial(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return View();
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * Buscamos la encuesta con el id recibido por GET y almacenamos el nombre de la encuesta en ViewBag junto con su id para despues mostrarlos en la vista
                 * Obtenemos todas las preguntas que pertenecen a una encuesta y enviamos el resultado a la vista
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var encuesta = db.encuestas.Find(id);

                    ViewBag.encuestaId = id;
                    ViewBag.encuestaName = encuesta.nombre;

                    var pregunta = db.preguntas.Where(a => a.encuesta_id.Equals(id)).ToList();
                   
                    return View(pregunta);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al capturar las preguntas " + ex.Message);
                return View();
            }
        }

        // GET /Preguntas/Create
        // Muestra la vista para crear las preguntas
        // Solo los Administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin","Encuesta");
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * Buscamos la encuesta con el id recibido por GET y almacenamos el nombre de la encuesta en ViewBag junto con su id para despues mostrarlos en la vista
                 * Mostramos la vista
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var encuesta = db.encuestas.Find(id);
                    ViewBag.encuestaId = id;
                    ViewBag.encuestaName = encuesta.nombre;

                    return View();
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al capturar las preguntas" + ex.Message);
                return RedirectToAction("Admin", "Encuesta");
            }
        }

        // POST /Preguntas/Create
        // Crea una pregunta
        // Solo los Administradores tienen acceso
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult create(preguntas data)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid )
            {
                return RedirectToAction("Index", "Preguntas", new { id = data.encuesta_id});
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * En el contexto de "preuntas" cargamos los datos que recibimos por POST y guardamos los cambios
                 * Finalmente redirigmos a /Preguntas/{id encuesta}
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    db.preguntas.Add(data);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Preguntas", new { id = data.encuesta_id });
                }
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al capturar las pregunta " + ex.Message);
                return RedirectToAction("Index", "Preguntas", new { id = data.encuesta_id });
            }
        }

        // GET: /Preguntas/Edit/{ID}
        // Muestra la vista para editar las categorías
        // Solo los administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin", "Encuestas");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * Buscamos la pregunta mediante el id que recibimos por GET
                 * Buscamos la encuesta a la que está relacionada la pregunta  y enviamos los resultados a la vista para que sean mostrados
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {   
                    preguntas preguntaTmp = db.preguntas.Find(id);
                    var encuesta = db.encuestas.Find(preguntaTmp.encuesta_id);
                   
                    ViewBag.encuestaId = encuesta.id;
                    ViewBag.encuestaName = encuesta.nombre;

                    
                    return View(preguntaTmp);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al mostrar la pregunta para edición" + ex.Message);
                return RedirectToAction("Admin", "Encuestas");
            }
        }

        // POST: /Preguntas/Edit
        // Actualiza la información de una pregunta
        // Solo los administradores tienen acceso
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(preguntas data)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", "Preguntas", new { id = data.encuesta_id });
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * Buscamos la pregunta mediante el id, posteriomente modificamos los campos necesarios en el modelo para poder guardar la información en la base de datos
                 * Finalmente redirigmos a /Preguntas/{id encuesta}
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    preguntas pregunta = db.preguntas.Find(data.id);
                    pregunta.pregunta = data.pregunta;
                    pregunta.encuesta_id = data.encuesta_id;
                    pregunta.status_id = data.status_id;
                    db.SaveChanges();

                    return RedirectToAction("Index", "Preguntas", new { id = data.encuesta_id });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al editar la pregunta " + ex.Message);
                return RedirectToAction("Index", "Preguntas", new { id = data.encuesta_id });
            }
        }

        // GET: /Preguntas/Edit/{ID}
        // Elimina Fisicamente una pregunta
        // Solo los administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin", "Encuestas");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * Buscamos la pregunta mediante el id que recibimos por GET, despues indicamos que pregunta de nuestro contexto db va a ser eliminado
                 * Finalmente guardamos los cambios y redirigimos a /Preguntas/{id encuesta}
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    preguntas pregunta = db.preguntas.Find(id);
                    var encuestaId = pregunta.encuesta_id;
                    db.preguntas.Remove(pregunta);
                    db.SaveChanges();

                    return RedirectToAction("Index", "Preguntas", new { id = encuestaId });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al eliminar la pregunta " + ex.Message);
                return RedirectToAction("Admin", "Encuestas");
            }
        }


        // GET: /Preguntas/FormPartialAnswers/{ID}
        // Es una vista partial que muestra las preguntas junto con un campo de texto para capturar la respuesta
        // Solo los administradores y encuestados tienen acceso
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult FormPartialAnswers(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return View();
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitirá conectar con la base de datos
                 * Buscamos la encuesta mediante el id que recibimos por GET, almacenamos el id y el nombre de la encuesta en un ViewBag para posteriormnete utilizarlos en la vista
                 * Buscamos todas las pregutnas que corresponden a la encuesta con el id que se recibio por GET
                 * Finalmente mostramos la vista parcial
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var encuesta = db.encuestas.Find(id);

                    ViewBag.encuestaId = id;
                    ViewBag.encuestaName = encuesta.nombre;

                    var pregunta = db.preguntas.Where(a => a.encuesta_id.Equals(id)).ToList();

                    return View(pregunta);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al mostrar las preguntas " + ex.Message);
                return View();
            }
        }
    }
}