﻿using ProyectoEncuestas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoEncuestas.Controllers
{
    public class StatusController : Controller
    {
        // GET: /Status
        // Es una vista parcial que muestra un combobox con la lista de estatus disponibles
        public ActionResult cmbStatus(int id = 1)
        {
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Obtiene la lista de estatus y envia el resultado a la vista
                 */
                ViewBag.statusId = id;
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    List<status> listStatus = db.status.ToList();
                    return PartialView(listStatus );
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al Mostrar la lista de estatus " + ex.Message);
                return PartialView(null);
            }
        }
    }
}