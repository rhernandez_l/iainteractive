﻿using ProyectoEncuestas.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProyectoEncuestas.Controllers.Api
{
    public class EncuestasController : ApiController
    {

        // GET: /Encuesta/Activas/{ID Categoria (opcional)}
        // Muestra las encuestas activas filtradas por categoría
        // Solo los administradores y encuestados tienen acceso
        public List<EncuestasCE> GetIndex(string id = "")
        {
            List<EncuestasCE> data = new List<EncuestasCE>();
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Ejecutamos un query en la base de datos, tratando los parametros para impedir sql injection
                 * Finalmente regresamos el resultado
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    string queryString = @"select distinct a.id,a.nombre,a.categoria_id, a.status_id,a.created_at,b.categoria as categoria_name,c.status as status_name
                        from encuestas as a 
                        inner join categorias b on a.categoria_id = b.id
                        inner join status c on a.status_id = c.id
                        where a.status_id = 1" + (id !="" ? " and b.categoria = @cat " : "") + @"
                        order by b.categoria, a.nombre";

                     data = db.Database.SqlQuery<EncuestasCE>(queryString, new SqlParameter("cat ", id)).ToList();
                    return data;
                }

            }
            catch (Exception )
            {
                return data;
            }
           
        }
    }
}
