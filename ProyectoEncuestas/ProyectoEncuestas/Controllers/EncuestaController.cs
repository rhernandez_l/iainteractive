﻿using Microsoft.AspNet.Identity;
using ProyectoEncuestas.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoEncuestas.Controllers
{
    public class EncuestaController : Controller
    {
        // GET: /Encuesta/Admin  
        // Muestra la lista de encuestas
        // Solo los Administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Admin()
        {
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Mediante LinQ obtenemos las encuestas con sus estatus y categorias, y los retornamos a la vista en forma de lista
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var data = from a in db.encuestas
                               join b in db.categorias on a.categoria_id equals b.id
                               join c in db.status on a.status_id equals c.id
                               select new EncuestasCE()
                               {
                                   id = a.id,
                                   nombre = a.nombre,
                                   categoria_id = a.categoria_id,
                                   status_id = a.status_id,
                                   created_at = a.created_at,
                                   categoria_name = b.categoria,
                                   status_name = c.status1
                               };


                    return View(data.ToList());
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al cargar la lista de encuestas " + ex.Message);
                return View();
            }
        }

        // GET: /Encuesta/Create
        // Muestra la vista para crear encuestas
        // Solo los Administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            // simplemente llamamos la vista que nos permitira crear una encuesta
            return View();
        }

        // POST Encuesta/Create
        // Crea una encuesta
        // Solo los Administradores tienen acceso
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(encuestas data)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Como no recibimos la fecha de creación desde la vista aquí es donde la establecemos con DateTime.Now
                 * En el contexto de "encuestas" cargamos los datos que recibimos por POST y guardamos los cambios, para finalmente redirigir a /Preguntas/Create
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    data.created_at = DateTime.Now;
                    db.encuestas.Add(data);
                    db.SaveChanges();

                    return RedirectToAction("Create", "Preguntas", new { id = data.id });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al agregar la categoría " + ex.Message);
                return RedirectToAction("Admin");
            }
        }

        // GET: /Encuesta/Edit/{ID}
        // Muestra la vista para editar las encuestas
        // Solo los administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            // validamos tambien que el id de la encuesta sea mayor que cero
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Buscamos la encuesta mediante el id que recibimos por GET y enviamos los resultados a la vista para que sean mostrados
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var encuesta = db.encuestas.Find(id);
                    return View(encuesta);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al mostrar la encuesta para edición" + ex.Message);
                return RedirectToAction("Admin");
            }
        }

        // POST: /Encuesta/Edit
        // Actualiza la información de una encuesta
        // Solo los administradores tienen acceso
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(encuestas data)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Admin");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Buscamos la encuesta mediante el id, posteriomente modificamos los campos necesarios en el modelo para poder guardar la información en la base de datos
                 * Finalmente redirigmos a /Encuesta/Admin
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    encuestas encuesta = db.encuestas.Find(data.id);
                    encuesta.nombre = data.nombre;
                    encuesta.categoria_id = data.categoria_id;
                    encuesta.status_id = data.status_id;
                    //   encuesta.created_at = data.created_at;
                    db.SaveChanges();

                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al editar la encuesta " + ex.Message);
                return View();
            }
        }

        // GET: /Encuesta/Edit/{ID}
        // Elimina Fisicamente una encuesta
        // Solo los administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Buscamos la encuesta mediante el id que recibimos por GET, despues indicamos que encuesta de nuestro contexto db va a ser eliminado
                 * Finalmente guardamos los cambios y redirigimos a /Encuesta/Admin
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var categoria = db.encuestas.Find(id);
                    db.encuestas.Remove(categoria);
                    db.SaveChanges();

                    return RedirectToAction("Admin");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al eliminar la categoría " + ex.Message);
                return View();
            }
        }

        // GET: /Encuesta/Details/{ID}
        // Muestra el detalle de una encuesta
        // Solo los administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * buscamos la encuesta que coincida con el id que recibimos por GET
                 * Establecemos la información en el modelo EncuestasCE y finalmente enviamos la información a la vsita
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var data = from a in db.encuestas
                               join b in db.categorias on a.categoria_id equals b.id
                               join c in db.status on a.status_id equals c.id
                               where a.id == id
                               select new EncuestasCE()
                               {
                                   id = a.id,
                                   nombre = a.nombre,
                                   categoria_id = a.categoria_id,
                                   status_id = a.status_id,
                                   created_at = a.created_at,
                                   categoria_name = b.categoria,
                                   status_name = c.status1
                               };

                    EncuestasCE encuesta = new EncuestasCE();
                    encuesta.id = data.First().id;
                    encuesta.nombre = data.First().nombre;
                    encuesta.categoria_id = data.First().categoria_id;
                    encuesta.status_id = data.First().status_id;
                    encuesta.created_at = data.First().created_at;
                    encuesta.categoria_name = data.First().categoria_name;
                    encuesta.status_name = data.First().status_name;
                    encuesta.created_at = data.First().created_at;

                    return View(encuesta);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al mostrar la encuesta " + ex.Message);
                return RedirectToAction("Admin");
            }
        }


        // GET: /Encuesta/Activas/{ID}
        // Muestra las encuestas activas filtradas por categoria
        // Solo los administradores y encuestados tienen acceso
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult Activas(int id = 0 )
        {

            ViewBag.categoria_id = id;
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Ejecutamos un query en la base de datos, tratando los parametros para impedir sql injection
                 * Finalmente enviamos el resultado del query a la vista
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    string queryString = @"select distinct a.id,a.nombre,a.categoria_id, a.status_id,a.created_at,b.categoria as categoria_name,c.status as status_name
                        from encuestas as a 
                        inner join categorias b on a.categoria_id = b.id
                        inner join status c on a.status_id = c.id
                        where a.status_id = 1" + (id >0? " and a.categoria_id = @catId " : "") + @"
                        order by b.categoria, a.nombre";
                    
                    var data = db.Database.SqlQuery<EncuestasCE>(queryString, new SqlParameter("catId ", id));

                    return View(data.ToList());
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al cargar la lista de encuestas " + ex.Message);
                return View();
            }
        }

        // GET: /Encuesta/FormAnswer/{ID}
        // Muestra la vista para contestar las preguntas de una  encuesta
        // Solo los administradores y encuestados tienen acceso
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult FormAnswer(int id = 0)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Admin");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Obtenemos la información que corresponde a la encuesta que queremos contestar
                 * EStablecemos la informacion en el modelo EncuestasCE
                 * Finalmente enviamos el resultado  a la vista
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var data = from a in db.encuestas
                               join b in db.categorias on a.categoria_id equals b.id
                               join c in db.status on a.status_id equals c.id
                               where a.id == id
                               select new EncuestasCE()
                               {
                                   id = a.id,
                                   nombre = a.nombre,
                                   categoria_id = a.categoria_id,
                                   status_id = a.status_id,
                                   created_at = a.created_at,
                                   categoria_name = b.categoria,
                                   status_name = c.status1
                               };

                    EncuestasCE encuesta = new EncuestasCE();
                    encuesta.id = data.First().id;
                    encuesta.nombre = data.First().nombre;
                    encuesta.categoria_id = data.First().categoria_id;
                    encuesta.status_id = data.First().status_id;
                    encuesta.created_at = data.First().created_at;
                    encuesta.categoria_name = data.First().categoria_name;
                    encuesta.status_name = data.First().status_name;
                    encuesta.created_at = data.First().created_at;

                    return View(encuesta);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al mostrar la encuesta " + ex.Message);
                return RedirectToAction("Admin");
            }
        }

        // POST: /Encuesta/FormAnswer
        // Guarda las respuestas de la encuesta que se contesto
        // Solo los administradores y encuestados tienen acceso
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult FormAnswer(FormCollection data)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            try
            {

                /*
                 * Obtenemos el id del usuario y el id de la encuesta
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Recorremos el array de todas las preguntas que fueron contestadas
                 * Cada respuesta la vamos almacenando temporalmente en el contexto respuestas
                 * Al final del ciclo guardamos las respuestas en la base de datos
                 */
                var userId = User.Identity.GetUserId();
                int encuestaId = int.Parse(data["encuesta_id"]);
                using (iainteractiveEntities db = new iainteractiveEntities())
                {

                    foreach (string key in data.AllKeys)
                    {
                        string[] isPregunta = key.Split('.');
                        if (isPregunta.Count() > 1)
                        {
                            var value = data[key];
                            var preguntaId = int.Parse(isPregunta[1]);

                            respuestas respuesta = new respuestas();
                            respuesta.encuesta_id = encuestaId;
                            respuesta.aspnetuser_id = userId;
                            respuesta.pregunta_id = preguntaId;
                            respuesta.respuesta = value;
                            respuesta.created_at = DateTime.Now;

                            db.respuestas.Add(respuesta);

                        }
                        
                    }
                    if (db.respuestas.Local.Count() > 0)
                    {
                        db.SaveChanges();
                    }
                
                    return RedirectToAction("Activas", "Encuesta", new { id = 1 });
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al agregar la categoría " + ex.Message);
                return RedirectToAction("Activas", "Encuesta", new { id = 1 });
            }
        }

        // POST: /Encuesta/MyAnswers
        // Permite que el usuario vea las encuestas que ha contestado
        // Solo los administradores y encuestados tienen acceso
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult MyAnswers()
        {
            try
            {
                /*
                 * Obtenemos el id del usuario 
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Ejecutamos un query que nos muestra la lista de todas las encuestas que ha contestado el usuario logueado
                 * Finalmente se envía la información a la vista
                 */
                var userId = User.Identity.GetUserId();
                using (var db = new iainteractiveEntities())
                {
                 
                    string queryString =
                                @"select distinct a.id,a.nombre,a.categoria_id, a.status_id,a.created_at,b.categoria as categoria_name,c.status as status_name
                        from encuestas as a 
                        inner join categorias b on a.categoria_id = b.id
                        inner join status c on a.status_id = c.id
                        inner join respuestas d on a.id = d.encuesta_id 
                        where d.aspnetuser_id = @userID
                        order by b.categoria, a.nombre";

             
                  
                    var data = db.Database.SqlQuery<EncuestasCE>(queryString, new SqlParameter("userID ", userId ));

                    return View(data.ToList());
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al cargar la lista de encuestas " + ex.Message);
                return View();
            }
        }
    }
}