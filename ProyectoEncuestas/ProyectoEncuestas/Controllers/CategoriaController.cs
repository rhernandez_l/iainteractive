﻿
using ProyectoEncuestas.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ProyectoEncuestas.Controllers
{
    public class CategoriaController : Controller
    {
      
        // GET: /Categoria  
        // Muestra la lista de categorías
        // Solo los Administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Mediante LinQ obtenemos las categorias con sus estatus y los retornamos a la vista en forma de lista
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var data = from a in db.categorias
                               join b in db.status 
                               on a.status_id equals b.id
                              
                               select new CategoriasCE()
                               {
                                   id = a.id,
                                   categoria = a.categoria,
                                   status_id = a.status_id,
                                   status_name = b.status1
                               };
                    return View(data.ToList());
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al cargar la lista de categorías " + ex.Message);
                return View();
            }
           
        }

        // GET Categoria/Create
        // Muestra la vista para crear las categorías
        // Solo los Administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            // simplemente llamamos la vista que nos permitira crear una categoria
            return View();
        }

        // POST Categoria/Create
        // Crea una categoría
        // Solo los Administradores tienen acceso
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(categorias data)
        {
            // Validamos que los parametros se recibieron (es una validación simple que por tiempo es lo que pudimos hacer )
            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * En el contexto de "categorías" cargamos los datos que recibimos por POST y guardamos los cambios, para finalmente redirigir a /Categoria
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    db.categorias.Add(data);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", "Error al crear la categoría"+ex.Message );
                return View();
            }

        }


        // GET: /Categoria/Edit/{ID}
        // Muestra la vista para editar las categorías
        // Solo los administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id = 0)
        {
            if (!ModelState.IsValid || id <1)
            {
                return RedirectToAction("Index");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Buscamos la categoria mediante el id que recibimos por GET y enviamos los resultados a la vista para que sean mostrados
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    var categoria = db.categorias.Find(id);
                    return View(categoria);
                }

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", "Error cargar la categoría " + ex.Message);
                return RedirectToAction("Index");
            }
        }

        // POST: /Categoria/Edit
        // Actualiza la información de una categoría
        // Solo los administradores tienen acceso
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(categorias data)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Buscamos la categoria mediante el id, posteriomente modificamos los campos necesarios en el modelo para poder guardar la información en la base de datos
                 * Finalmente redirigmos a /Categoria
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    categorias categoria = db.categorias.Find(data.id);
                    categoria.categoria = data.categoria;
                    categoria.status_id = data.status_id;

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", "Error al editar la categoría " + ex.Message);
                return View();
            }

        }

        // GET: /Categoria/Edit/{ID}
        // Elimina Fisicamente una categoría
        // Solo los administradores tienen acceso
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id = 0 )
        {
            if (!ModelState.IsValid || id < 1)
            {
                return RedirectToAction("Index");
            }
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Buscamos la categoria mediante el id que recibimos por GET, despues indicamos que categoria de nuestro contexto db va a ser eliminado
                 * Finalmente guardamos los cambios y redirigimos a /Categoria
                 */
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    categorias categoria = db.categorias.Find(id);
                    db.categorias.Remove(categoria);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", "Error eliminar la categoría " + ex.Message);
                return View();
            }
        }


        // GET: /Categoria/Edit/{ID}
        // Es una vista parcial la cual muestra un combo Box que contiene la lista de todas las categorías
        // Solo los administradores y encuestados tienen acceso
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult cmbCategorias(int id = 0)
        {
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Almacenamos el id de la categoria dentro de un ViewBag para posteriormente utilizarlo en la vista
                 * Obtenemos la lista de todas las categorias y enviamos los resultados a la vista parcial
                 * Finalmente guardamos los cambios y redirigimos a /Categoria
                 */
                ViewBag.categoriaId = id;
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    List<categorias> listCategorias = db.categorias.ToList();
                    return PartialView(listCategorias);
                }
            } 
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al Mostrar la lista de categorias " + ex.Message);
                return PartialView(null);
            }
        }

        // GET: /Categoria/Edit/{ID}
        // Es una vista parcial la cual muestra un combo Box que contiene la lista de todas las categorías pero se agrega la opcion "- Seleccione - " la cual contiene el valor = 0
        // Solo los administradores y encuestados tienen acceso
        [Authorize(Roles = "Administrador,Encuestados")]
        public ActionResult cmbCategorias_WithSelect(int id = 0)
        {
            try
            {
                /*
                 * Cargarmos el context que nos permitira conectar con la base de datos
                 * Almacenamos el id de la categoria dentro de un ViewBag para posteriormente utilizarlo en la vista
                 * Obtenemos la lista de todas las categorias y enviamos los resultados a la vista parcial
                 * Finalmente guardamos los cambios y redirigimos a /Categoria
                 */
                ViewBag.categoriaId = id;
                using (iainteractiveEntities db = new iainteractiveEntities())
                {
                    List<categorias> listCategorias = db.categorias.ToList();
                    return PartialView(listCategorias);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al Mostrar la lista de categorias " + ex.Message);
                return PartialView(null);
            }
        }
    }


}
 