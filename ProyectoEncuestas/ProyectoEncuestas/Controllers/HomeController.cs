﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoEncuestas.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Title = "Acerca de...";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Title = "Contacto.";

            return View();
        }
    }
}