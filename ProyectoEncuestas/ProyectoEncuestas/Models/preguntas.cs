//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProyectoEncuestas.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class preguntas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public preguntas()
        {
            this.respuestas = new HashSet<respuestas>();
        }
    
        public int id { get; set; }
        public string pregunta { get; set; }
        public int encuesta_id { get; set; }
        public byte status_id { get; set; }
    
        public virtual encuestas encuestas { get; set; }
        public virtual status status { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<respuestas> respuestas { get; set; }
    }
}
