﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ProyectoEncuestas.Models
{
    // Está clase se creo para poder mostrar las categorías con sus estatus
    public class CategoriasCE
    {
        public int id { get; set; }

        [Required]
        [Display(Name = "Categoría")]
        public string categoria { get; set; }

        [Required]
        [Display(Name = "Estatus")]
        public byte status_id { get; set; }

        [Display(Name = "Estatus")]
        public string status_name { get; set; } 
    }

}