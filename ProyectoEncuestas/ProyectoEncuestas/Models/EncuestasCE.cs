﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProyectoEncuestas.Models
{
    // Está clase se creo para poder mostrar las encuestas con sus estatus y categorías
    public class EncuestasCE
    {
        public int id { get; set; }

        [Display(Name = "Categoría")]
        public int categoria_id { get; set; }
        [Display(Name ="Nombre de la encuesta")]
        public string nombre { get; set; }

        [Display(Name = "Estatus")]
        public byte status_id { get; set; }

        [Display(Name = "Fecha")]
        public System.DateTime created_at { get; set; }

        [Display(Name ="Categoría")]
        public string categoria_name { get; set; }
        [Display(Name ="Estatus")]
        public string status_name { get; set; }
    }
}