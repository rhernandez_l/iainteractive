﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
namespace ProyectoEncuestas.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Por favor proporcione su Nombre.")]
        [StringLength(100, ErrorMessage = "El {0} debe contener cuando menos de {2} caracteres.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Por favor proporcione sus Apellidos.")]
        [StringLength(100, ErrorMessage = "El {0} debe contener cuando menos de {2} caracteres.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Apellidos")]
        public string Apellidos { get; set; }


        [Required(ErrorMessage = "Por favor seleccione un Genero.")]
        [DataType(DataType.Text)]
        [Display(Name = "Genero")]
        public string Genero { get; set; }

        [Required(ErrorMessage = "Por favor proporcione su Email.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Por favor proporcione una contraseña.")]
        [StringLength(100, ErrorMessage = "El {0} debe contener cuando menos de {2} caracteres.", MinimumLength = 6)]
        /*[DataType(DataType.Password)]*/
        [RegularExpression("^(?=.*[0-9])(?=.*[!@#$%^&*])[0-9a-zA-Z!@#$%^&*0-9]{10,}$", ErrorMessage = "La contraseña debe de contener una letra mayuscula, una letra minuscula, un digito y un caracter especial. Ejm. Asd123456@@ ")]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        /*[DataType(DataType.Password)]*/
        [RegularExpression("^(?=.*[0-9])(?=.*[!@#$%^&*])[0-9a-zA-Z!@#$%^&*0-9]{10,}$", ErrorMessage = "La contraseña debe de contener una letra mayuscula, una letra minuscula, un digito y un caracter especial. Ejm. Asd123456@@ ")]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "La confirmación de contraseña no coincide.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
